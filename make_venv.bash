#!/bin/bash

# This script must be "sourced".
# One reason is because it enables a Python Virtual environment in the current shell (not a subshell)

# Developer Note: To exit gracefully (whether script was executed directly or sourced):
#     return -1 2> /dev/null || exit -1

export PYTHONDONTWRITEBYTECODE=true  # We don't want bytecode in our project.

# Session Variables
envdir="pyvenv.d"
python_path="/usr/local/bin/python3.7"

# If argument '--reinstall', remove existing Python virtual environment.
if [[ $1 = '--reinstall' ]] ; then
    if [[ "$VIRTUAL_ENV" != '' ]]; then
        deactivate
    fi
    echo "Removing previous Python virtual environment..."
    rm -rf $envdir
fi

bash -c "echo && $python_path --version"

# If no Python virtual environment exists, create it.
if [ ! -d "$envdir" ]; then
    echo -e "\nCreating a new Python Virtual environment ($envdir) ..."
    bash -c "$python_path -m venv $envdir"
    echo -e "[global]\ndisable-pip-version-check = True" > "$envdir/pip.conf"
    echo "...complete."
fi

# Activate Python virtual environment, if not already active.
if [[ "$VIRTUAL_ENV" == '' ]]; then
  source "$envdir/bin/activate"
  echo -e "Python virtual environment is active."
fi

# Default pip logfile should be in the venv directory
export PIP_LOG_FILE="$VIRTUAL_ENV/pip_log.txt"

# Cleanup Variables
unset envdir
unset python_path

# Update to latest version of pip.
pip install --quiet --upgrade pip
pip install --quiet --upgrade setuptools wheel flake8 flake8-tabs pytest
