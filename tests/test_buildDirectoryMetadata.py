# Standard Python modules
import pathlib
# PyPI
import pytest
import pprint
# Local test data
import sample_data_lib as sample
# Module we're testing
import file8601


# Temporary Directory for all the Sample data.
@pytest.fixture(scope="session")
def sample_data_dir(tmp_path_factory):
	datadir_path = tmp_path_factory.mktemp("data")
	# TOML
	sample.create_toml_file(datadir_path)
	sample.create_txt_files(datadir_path)
	sample.create_tar_gz_files(datadir_path)
	return datadir_path


def test_valid_directory(sample_data_dir):
	result_dict = file8601.Directory(sample_data_dir).build_directory_metadata()
	assert len(result_dict['files']) == 96


def test_valid_directory_with_glob(sample_data_dir):
	"""Only parse files that meet criteria *.tar.gz"""
	test_glob = ('*.tar.gz',)
	result_dict = file8601.Directory(sample_data_dir).build_directory_metadata(test_glob)
	assert len(result_dict['files']) == 72


def test_valid_directory_with_badglob(sample_data_dir):
	"""The single *.toml file in the directory should be gracefully ignored."""
	test_glob = ('*.toml',)
	result_dict = file8601.Directory(sample_data_dir).build_directory_metadata(test_glob)
	assert len(result_dict['files']) == 0


def test_invalid_directory(sample_data_dir):
	test_directory = pathlib.Path(__file__).parent / 'whatchamacalit'
	with pytest.raises(FileNotFoundError):
		file8601.Directory(test_directory).build_directory_metadata()


def tmp_buildSortData(sample_data_dir):
	metadata = file8601.Directory(sample_data_dir).build_directory_metadata()
	metadata = file8601.add_sortcodes(metadata)
	
	pprint_instance = pprint.PrettyPrinter(indent=4)
	print()
	pprint_instance.pprint(metadata)
	print()


if __name__ == "__main__":
	pytest.main(["-x", "test_buildDirectoryMetadata.py"])

