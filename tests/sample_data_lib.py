# Standard modules
import datetime
import pathlib
# PyPI
from dateutil import tz
# Module we're testing:
import file8601


def create_toml_file(dir_path):
	toml = """[sample_group]
description="Just some ordinary TOML data"
sample_data="Hello World"
"""
	file_path = pathlib.Path(dir_path) / 'sample_toml_file.toml'
	file_conn = open(file_path, 'a')
	file_conn.write(toml)
	file_conn.close()
	return file_path


def create_txt_files(dir_path):
	""" Create sample files with varying datetimes and time zones"""
	base_filename = 'mysql_sf.txt'
	filecount = 0

	# 24 files for Los Angeles
	timezone_LAX = tz.gettz('America/Los Angeles')
	start_datetime = datetime.datetime(year=1955, month=11, day=5,
	                                   hour=15, minute=10, tzinfo=timezone_LAX)
	for i in range(0, 24, 2):  # 1 hour for 48 hours
		new_filename = file8601.StampFilename(dir_path / base_filename,
		                                      start_datetime + datetime.timedelta(hours=i)).add_suffix()
		open(new_filename, 'a').close()
		filecount += 1

	# 24 files for New York
	timezone_NYC = tz.gettz('America/New York')
	start_datetime = datetime.datetime(year=1955, month=11, day=5,
	                                   hour=15, minute=10, tzinfo=timezone_NYC)
	for j in range(1, 25, 2):  # 1 hour for 48 hours
		new_filename = file8601.StampFilename(dir_path / base_filename,
		                                      start_datetime + datetime.timedelta(hours=j)).add_suffix()
		open(new_filename, 'a').close()
		filecount += 1
	print(f'Created {filecount} new .txt files.')


def create_tar_gz_files(dir_path):
	""" Create sample files with varying datetimes and time zones"""
	base_filename = 'sample_backup.tar.gz'
	filecount = 0
	timezone_NYC = tz.gettz('America/Chicago')
	start_datetime = datetime.datetime(year=1955, month=11, day=1,
	                                   hour=15, minute=10, tzinfo=timezone_NYC)
	for j in range(0, 72, 1):  # 1 hour for 72 hours
		new_filename = file8601.StampFilename(dir_path / base_filename,
		                                      start_datetime + datetime.timedelta(hours=j)).add_suffix()
		open(new_filename, 'a').close()
		filecount += 1
	print(f'Created {filecount} new .tar.gz files.')
