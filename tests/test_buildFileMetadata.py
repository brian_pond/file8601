# Standard Python modules
import datetime
import os
import pathlib
import logging
# PyTest
import pytest
from dateutil import tz
# Local test data
import sample_data_lib as sample
# Module we're testing
import file8601

# Configure Python logging
logging.basicConfig(level=os.environ.get("LOGLEVEL", "INFO"))
logger = logging.getLogger(__name__)

# Temporary Directory for all the Sample data.
@pytest.fixture(scope="session")
def sample_data_dir(tmp_path_factory):
	datadir_path = tmp_path_factory.mktemp("data")
	# TOML
	sample.create_toml_file(datadir_path)
	sample.create_txt_files(datadir_path)
	sample.create_tar_gz_files(datadir_path)
	return datadir_path


def test_single_extension(sample_data_dir):
	""" Test a valid file8601 file name. """
	test_file = pathlib.Path(sample_data_dir) / 'mysql_sf_1955-11-05T211000-0800.txt'
	# logger.debug(f"Test file: {test_file}")
	if not test_file:
		raise Exception("Could not find a file with extension '.txt'")

	expected_dict = {'datetime_string': '1955-11-05T211000-0800',
	                 'full_name': 'mysql_sf_1955-11-05T211000-0800.txt',
	                 'orig_datetime': datetime.datetime(1955, 11, 5, 21, 10, tzinfo=tz.tzoffset(None, -28800)),
	                 'parentdir': str(test_file.parent),
	                 'path': pathlib.PosixPath(test_file.resolve()),
	                 'prefix': 'mysql_sf',
	                 'suffix': '.txt',
	                 'utc_date': datetime.date(1955, 11, 6),
	                 'utc_datetime': datetime.datetime(1955, 11, 6, 5, 10, tzinfo=tz.tzfile('/usr/share/zoneinfo/UTC')),
	                 'utc_time': datetime.time(5, 10)}

	result = file8601.build_file_metadata(test_file, False)
	assert result == expected_dict


def test_double_extension(sample_data_dir):
	""" Test a valid file8601 file name. """
	test_file = pathlib.Path(sample_data_dir) / 'mysql_sf_1955-11-05T211000-0800.tar.gz'
	if not test_file:
		raise Exception("Unable to create pathlib.Path in function test_double_extension()")

	expected_dict = {'datetime_string': '1955-11-05T211000-0800',
	                 'full_name': 'mysql_sf_1955-11-05T211000-0800.tar.gz',
	                 'orig_datetime': datetime.datetime(1955, 11, 5, 21, 10, tzinfo=tz.tzoffset(None, -28800)),
	                 'parentdir': str(test_file.parent),
	                 'path': pathlib.PosixPath(test_file.resolve()),
	                 'prefix': 'mysql_sf',
	                 'suffix': '.tar.gz',
	                 'utc_date': datetime.date(1955, 11, 6),
	                 'utc_datetime': datetime.datetime(1955, 11, 6, 5, 10, tzinfo=tz.tzfile('/usr/share/zoneinfo/UTC')),
	                 'utc_time': datetime.time(5, 10)}

	result = file8601.build_file_metadata(test_file, False)
	assert result == expected_dict


def test_invalid_file(sample_data_dir):
	""" Test filename that does not end with a valid ISO-8601 datetime"""
	file_path = pathlib.Path(sample_data_dir) / 'sample_toml_file.toml'
	with pytest.raises(ValueError, match=r"ISO 8601"):
		file8601.build_file_metadata(file_path, True)


if __name__ == "__main__":
	pytest.main(["-x", "test_buildFileMetadata.py"])
