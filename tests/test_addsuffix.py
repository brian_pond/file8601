# Standard modules
import datetime
import pathlib
# PyPI packages
from dateutil import tz
import pytest
# Package to be tested
import file8601

# Usage: '$ pytest --verbose'


def test_wrong_filename_type():
	with pytest.raises(TypeError, match=r"type String or pathlib.Path"):
		file8601.StampFilename(42).add_suffix()


def test_missing_timezone():
	"""Do not accept a naive datetime object."""
	test_datetime = datetime.datetime(year=1955, month=11, day=5, hour=15, minute=10)
	with pytest.raises(ValueError, match=r"time zone"):
		file8601.StampFilename('some name', test_datetime).add_suffix()


def test_string_suffix():
	test_filename = 'some_file_name.txt'
	timezone_LAX = tz.gettz('America/Los Angeles')
	test_datetime = datetime.datetime(year=1955, month=11, day=5,
	                                  hour=15, minute=10, tzinfo=timezone_LAX)
	expected_result = 'some_file_name_1955-11-05T151000-0800.txt'
	result = file8601.StampFilename(test_filename, test_datetime).add_suffix()
	assert result == expected_result


def test_string_prefix():
	test_filename = 'some_file_name.txt'
	timezone_LAX = tz.gettz('America/Los Angeles')
	test_datetime = datetime.datetime(year=1955, month=11, day=5,
	                                  hour=15, minute=10, tzinfo=timezone_LAX)
	expected_result = '1955-11-05T151000-0800_some_file_name.txt'
	actual_result = file8601.StampFilename(test_filename, test_datetime).add_prefix()
	assert expected_result == actual_result


def test_path_suffix():
	test_filename = pathlib.Path().cwd() / 'test_files' / 'sample_test_file.toml'
	timezone_LAX = tz.gettz('America/Los Angeles')
	test_datetime = datetime.datetime(year=1955, month=11, day=5,
	                                  hour=15, minute=10, tzinfo=timezone_LAX)
	expected_result = pathlib.Path().cwd() / 'test_files' / 'sample_test_file_1955-11-05T151000-0800.toml'
	assert file8601.StampFilename(test_filename, test_datetime).add_suffix() == expected_result


def test_multiple_extensions():
	""" Code must correctly handle files with many suffixes."""
	# Example below is a real filename for OpenSSL.
	test_filename = 'openssl-1.1.1d.tar.gz'
	timezone_LAX = tz.gettz('America/Los Angeles')
	test_datetime = datetime.datetime(year=1955, month=11, day=5,
	                                  hour=15, minute=10, tzinfo=timezone_LAX)
	expected_result = 'openssl-1.1.1d_1955-11-05T151000-0800.tar.gz'
	actual_result = file8601.StampFilename(test_filename, test_datetime).add_suffix()
	assert expected_result == actual_result


if __name__ == "__main__":
	pytest.main(["-x", "test_addsuffix.py"])

# Fun Fact:  The datetime used in testing is a combination of 'Back to the Future' and '3:10 to Yuma'
