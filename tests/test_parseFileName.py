import datetime
import dateutil.parser
from dateutil.tz import tzoffset
import file8601
import os
import pathlib
import pytest
import re

import logging
# Configure Python logging
logging.basicConfig(level=os.environ.get("LOGLEVEL", "INFO"))
logger = logging.getLogger(__name__)


def test_regex():
	test_filename = '/home/think/mysql_sf_1955-11-05T211000-0800.tar.gz'
	mytz = tzoffset(None, -28800)
	expected_datetime = datetime.datetime(year=1955, month=11, day=5, hour=21, minute=10, tzinfo=mytz)
	logger.warning(f'Expected datetime: {expected_datetime}')
	file_path = pathlib.Path(test_filename)

	pattern = file8601.get_datetime_pattern()
	match_object = re.search(pattern, file_path.name)
	if not match_object:
		raise Exception("Could not find an ISO 8601 pattern in that string.")

	file_datetime_str = file_path.name[match_object.start():match_object.end()]
	file_datetime = dateutil.parser.parse(file_datetime_str)
	
	logger.warning(f'Actual datetime: {expected_datetime}')
	assert file_datetime == expected_datetime


if __name__ == "__main__":
	pytest.main(["-x", "test_parseFileName.py"])
